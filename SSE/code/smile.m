%Bouchard-Sorrnett法得出的V0-X
%结合Black-Scholes理论
%得出的波动率微笑图样

%全局一致参数
x0=500;
T=21;
% X0=500;

%波动率初始化
VX=V0_X;%参考V0_X.m
dimen=size(VX);
nrow=dimen(1);
value=zeros(nrow,1);

%解出波动率
test1=zeros(nrow,3);
for i=1:nrow
% for i=300;
    %二分法解sigma
    %尝试解区间的两个顶点的函数值fa,fb
    min=10^(-50);max=1;
    fa=V0_DIFF(VX(i,1),VX(i,2),min,x0,T);
    fb=V0_DIFF(VX(i,1),VX(i,2),max,x0,T);
%     test1(i,1)=VX(i,1);
%     test1(i,2)=fa;
%     test1(i,3)=fb;
    if fa*fb>0
        value(i)=0;
    end

    f=1;g=0;
    while abs(g-f)>abs(10^(-20))
        g=f;
        sigmaBS=(max+min)/2;
        f=V0_DIFF(VX(i,1),VX(i,2),sigmaBS,x0,T);%参考V0_DIFF.m
        if f*fa>0
            min=sigmaBS;
        else if f*fa<0;
                max=sigmaBS;
            else if f*fa==0;
                end
            end
        end
        value(i)=sigmaBS;
    end
end

%画出波动率变化曲线
figure;
scatter(VX(:,1),value,'.','b');
% axis([420 580 0.013 0.025]);
axis([300 700 0.016 0.028]);
% axis([420 580 0.018 0.023]);%<550和>1100的部分怎么了？ 猜测受Black-Scholes预测的取值范围影响
xlabel('strike price X');
ylabel('implied volatility sigma(imp)');
    
%     %V0与VX(i,2)比较测试 i=300
%     test=zeros(300,7);
%     for j=1:300
%         sigmaBS=j/1000.0;
%         d1=(log(x0/VX(i,1))+0.5*sigmaBS^2*T)/(sigmaBS*sqrt(T))
%         d2=(log(x0/VX(i,1))-0.5*sigmaBS^2*T)/(sigmaBS*sqrt(T))
%         m1=normcdf(d1,0,1);
%         m2=normcdf(d2,0,1);
%         V0=x0*normcdf(d1,0,1)-VX(i,1)*normcdf(d2,0,1);
%         min=abs(V0-VX(i,2));
%         test(j,1)=sigmaBS;
%         test(j,2)=d1;
%         test(j,3)=d2;
%         test(j,4)=m1;
%         test(j,5)=m2;
%         test(j,6)=V0;
%         test(j,7)=min;
%     end
    
%     fu=@(sigmaBS)x0*normcdf((log(x0/VX(i,1))+0.5*sigmaBS^2*T)/(sigmaBS*sqrt(T)),0,1)-VX(i,1)*normcdf((log(x0/VX(i,1))-0.5*sigmaBS^2*T)/(sigmaBS*sqrt(T)),0,1)-VX(i,2);
%     value(i)=fsolve(fu,0);

    %尝试解
%     sigma=0.1;
%     d1=(log(x0/VX(i,1))+0.5*sigma^2*T)/(sigma*sqrt(T));
%     d2=(log(x0/VX(i,1))-0.5*sigma^2*T)/(sigma*sqrt(T));
%     V0=x0*normcdf(d1,0,1)-VX(i,1)*normcdf(d2,0,1);
%     min=abs(V0-VX(i,2));
% 
%     for sigma=0:0.00001:0.01
%         d1=(log(x0/VX(i,1))+0.5*sigma^2*T)/(sigma*sqrt(T));
%         d2=(log(x0/VX(i,1))-0.5*sigma^2*T)/(sigma*sqrt(T));
%         V0=x0*normcdf(d1,0,1)-VX(i,1)*normcdf(d2,0,1);
%         if (V0-VX(i,2))<min
%             min=(V0-VX(i,2));
%             value(i)=sigma;
%         end
%     end
%end

