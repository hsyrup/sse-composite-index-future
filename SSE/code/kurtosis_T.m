function result=kurtosis_T

clear XT
T=21;%常数1,期货时间长度

%加载日线数据
% data=xlsread('data4.xlsx','C1:C242');
%data=load('data2011_2015.txt');
data=load('data95_15.txt');
data=data(:,5);

%计算XT的峰度
q=T;
nrow=size(data);
length=nrow(1);
XT=zeros(length-q,1);
for i=1:length-q
    XT(i)=(data(q+i)-data(i));%/data(i);
end
XTavg=mean(XT);%%%%%%%%%%


XT4=zeros(length-q,1);
XT2=zeros(length-q,1);
for i=1:length-q
    XT4(i)=(XT(i)-XTavg)^4;
    XT2(i)=(XT(i)-XTavg)^2;
end
sqrt(mean(XT2))
result=mean(XT4)/mean(XT2)^2;



for i=1:length-q
    XT(i)=XT(i)-XTavg;
end

%以下为摘抄自别处的密度计算 变量名无意义
Rt2=XT;
%分区间
Rmax=max(Rt2);
Rmin=min(Rt2);
dR=(Rmax-Rmin)/sqrt(length-q);

%n[R']分区间计数
num=floor((Rmax-Rmin)/dR)+1;
nR=zeros(num,1);%每个区间的计数
for i=0:num-1
    for j=1:length-q
        if Rt2(j)>=Rmin+dR*i && Rt2(j)<Rmin+dR*(i+1)
            nR(i+1)=nR(i+1)+1;
        end
    end
end

%n[R']/n 各区间频度
for i=1:num
    nR(i)=nR(i)/(length-q);
end

%画出p[R']dR的柱状图
figure;
hist(Rt2,round(sqrt(length-q)))
xlabel('x(t+1)-xt');
ylabel('n[x(t+1)|xt]');




