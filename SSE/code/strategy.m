%对冲策略对比

%参数
X=800;
T=21;%期货时间长度
t=20;%当前时间
x0=800;

%加载日线数据
% data=xlsread('data4.xlsx','C1:C242');
%data=load('data2011_2015.txt');
data=load('data95_15.txt');
data=data(:,5);

%计算Rt    p[xT|xt]
nrow=size(data);
length=nrow(1);
length2=length-T;
Rt=zeros(length-T,1);%未去均值的return序列
for i=1:length-T
    Rt(i)=(data(i+T)-data(i+t))/data(i+t);%修改的位置 p[xT|xt]
end

%计算Rt2
Rt2=zeros(length-T,1);%去均值后的return序列
Rtavg=mean(Rt);
for i=1:length-T
    Rt2(i)=Rt(i)-Rtavg;
end

%分区间
Rmax=max(Rt2);
Rmin=min(Rt2);
dR=(Rmax-Rmin)/sqrt(length-T);

%n[R']分区间计数
num=floor((Rmax-Rmin)/dR)+1;
nR=zeros(num,1);%每个区间的计数
for i=0:num-1
    for j=1:length-T
        if Rt2(j)>=Rmin+dR*i && Rt2(j)<Rmin+dR*(i+1)
            nR(i+1)=nR(i+1)+1;
        end
    end
end

%n[R']/n 各区间频度
for i=1:num
    nR(i)=nR(i)/(length-T);
end

%画出p[R']dR的柱状图
% figure;
% hist(Rt2,round(sqrt(length-T)))

%画出p[R']dR的折线图
% figure;
% plot(Rmin:dR:Rmax,nR)

pRfunc(:,1)=Rmin:dR:Rmax;
pRfunc(:,2)=nR;%返回p[R']dR

% %读取return分布
% [pRfunc,~,Rmin,dR,~]=pR_func;
% dimen=size(pRfunc);%pRfunc是p[R']dR数据
% num=dimen(1);%num是柱状密度函数图的柱形数

%每个return分布区间的价格下限
x=zeros(num,1);
for i=0:num-1    
    x(i+1)=(1+Rmin+i*dR)
end


%起始期权价值(期权定价)V0 对 期货标价
boso_t=zeros(900,2);
blsc_t=zeros(900,2);
sigmaBS=0.24;
for xt=X-450:1:X+449

    boso_t(xt-X+451,1)=xt;%期货标价
    result=0;
    for i=1:num
        result=result+heaviside(xt*x(i)-X)*pRfunc(i,2)*(xt^3*dR^2/3-xt*dR/2*(xt+X-xt*x(i)*2)+(xt*x(i)-X)*(xt*x(i)-xt));
    end
    boso_t(xt-X+451,2)=result/(T-t)/xt^2/sigma_t_1;%期权定价 %待修改的sigma[Rt]
    
    
    blsc_t(xt-X+451,1)=xt;%期货标价
    d1=(log(xt/X)+0.5*sigmaBS^2*(T-t))/(sigmaBS*sqrt(T-t));
    blsc_t(xt-X+451,2)=normcdf(d1,0,1);
    
    
end

%画出boso_t~xt
% figure;
scatter(boso_t(:,1),boso_t(:,2),'.','b');
hold on;
scatter(blsc_t(:,1),blsc_t(:,2),'.','r');