function result=sigma_t_1

clear

T=21;%常数1,期货时间长度
%加载日线数据
% data=xlsread('data4.xlsx','C1:C242');
%data=load('data2011_2015.txt');
data=load('data95_15.txt');
data=data(:,5);

q=1;
nrow=size(data);
length=nrow(1);
for i=1:length-q
    XT(i)=(data(q+i)-data(i))/data(i);
end
result=sqrt(var(XT));

% XTavg=mean(XT);
% XT2=zeros(length-q,1);
% for i=1:length-q
%     XT2(i)=(XT(i)-XTavg)^2;
% end
% result=sqrt(mean(XT2))
