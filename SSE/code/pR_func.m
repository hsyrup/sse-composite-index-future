function [result,Rmax,Rmin,dR,length2,std]=pR_func

T=21;%常数1,期货时间长度

%加载日线数据
% data=xlsread('data4.xlsx','C1:C242');
% data=load('data2011_2015.txt');
% data=load('data95_15.txt');
% data=data(:,5);

% data=xlsread('compare.xlsx','B1:B1199');%缓升

% data=xlsread('compare.xlsx','E1:E1199');%缓降
% data=xlsread('compare.xlsx','N1:N1199');

% data=xlsread('compare.xlsx','H1:H499');%急升
% data=xlsread('compare.xlsx','Q1:Q499');

data=xlsread('compare.xlsx','K1:K1199');%剧烈波动



%计算Rt
q=21;
nrow=size(data);
length=nrow(1);
length2=length-q;
Rt=zeros(length-q,1);%未去均值的return序列
for i=1:length-q
    Rt(i)=(data(i+q)-data(i))/data(i);
end

%计算Rt2
Rt2=zeros(length-q,1);%去均值后的return序列
Rtavg=mean(Rt);
for i=1:length-q
    Rt2(i)=Rt(i)-Rtavg;
end

%分区间
Rmax=max(Rt2);
Rmin=min(Rt2);
dR=(Rmax-Rmin)/sqrt(length-q);

%n[R']分区间计数
num=floor((Rmax-Rmin)/dR)+1;
nR=zeros(num,1);%每个区间的计数
for i=0:num-1
    for j=1:length-T
        if Rt2(j)>=Rmin+dR*i && Rt2(j)<Rmin+dR*(i+1)
            nR(i+1)=nR(i+1)+1;
        end
    end
end

%n[R']/n 各区间频度
for i=1:num
    nR(i)=nR(i)/(length-q);
end

std=sqrt(var(nR))


%画出p[R']dR的柱状图
% figure;
% hist(Rt2,round(sqrt(length-T)))
% xlabel('return R(t,t-T)');
% ylabel('n[R(t,t-T)]');

%画出p[R']dR的折线图
% figure;
% plot(Rmin:dR:Rmax,nR,'LineWidth',2)
% xlabel('return R(t,t-T)');
% ylabel('p[R(t,t-T)]');
result(:,1)=Rmin:dR:Rmax;
result(:,2)=nR;%返回p[R']dR

