% function result=V0_X(x0,X)
function result2=V0_X

x0=500;%起始日上证指数的价格
X0=350;%期货标价测试的起始值
T=21;

%自变量初始价格
[pRfunc,~,Rmin,dR,~,sigma_1]=pR_func;
dimen=size(pRfunc);%pRfunc是p[R']dR数据
num=dimen(1);%num是柱状密度函数图的柱形数

%每个return分布区间的价格下限
x=zeros(num,1);
for i=0:num-1    
    x(i+1)=x0*(1+Rmin+i*dR);
end

%起始期权价值(期权定价)V0 对 期货标价
result2=zeros(400,3);
% sigma_1=0.019;
for X=X0:1:X0+399
    result2(X-X0+1,1)=X;%期货标价
    result=0;
    for i=1:num
        result=result+heaviside(x(i)-X)*pRfunc(i,2)*(x(i)-X+x0*dR/2);%……
    end
    result2(X-X0+1,2)=result;%期权定价
    
    result2(X-X0+1,3)=x0*normcdf((log(x0/X)+0.5*sigma_1^2*T)/sigma_1/sqrt(T),0,1)-X*normcdf((log(x0/X)-0.5*sigma_1^2*T)/sigma_1/sqrt(T),0,1);
end

%画出V0~X
figure;
plot(result2(:,1),result2(:,2),'b','LineWidth',2);
hold on;
plot(result2(:,1),result2(:,3),'r','LineWidth',2);
% axis([350 650 0 150]);
%axis([450 550 0 60]);
xlabel('strike price X');
ylabel('option price V0');
legend('V0 Bouchaud-Soronette','V0 Black-Scholes');

