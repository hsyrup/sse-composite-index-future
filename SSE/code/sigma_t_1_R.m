function result=sigma_t_1_R

T=21;%常数1,期货时间长度
x0=800;

%加载日线数据
% data=xlsread('data4.xlsx','C1:C242');
%data=load('data2011_2015.txt');
data=load('data95_15.txt');
data=data(:,5);

m=1;

%计算Rt
nrow=size(data);
length=nrow(1);
Rt=zeros(length-m,1);%未去均值的return序列
for i=1:length-m
    Rt(i)=(data(i+m)-data(i))/data(i);
end

%计算[Rt,t-1]的标准差
Rt22=zeros(length-m,1);%去均值后的return序列
Rtavg=mean(Rt);
for i=1:length-m
    Rt22(i)=(Rt(i)-Rtavg)^2;
end
result=mean(Rt22)*x0;
