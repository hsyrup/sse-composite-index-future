%sigma_imp

x0=500;
T=21;
%sigmaBS=sigma_t_1_R*x0;%参考sigma_t_1_R.m 间隔一天的return密度分布
sigmaBS=0.0174;%待考察的数值？？？？
%K=kurtosis_T;%参考kurtosis_T.m 间隔T天的差价分布的峰度/方差^2
K=8.9972;
sigma_T1=43.5361;
sigma_T21=225.5749;

sigma_imp_result=zeros(501,2);
for X=250:750
    sigma_imp_result(X-249,1)=X;
    sigma_imp_result(X-249,2)=sigmaBS*(1+(K-3)/24/T*((X-x0)^2/sigma_T1^2/T-1));%%参考sigma_t_1.m 间隔一天的差价密度分布
%     sigma_imp_result(X-349,2)=sigmaBS*(1+(K-3)/24*((X-x0)^2/sigma_T21^2-1));
end

figure;
plot(sigma_imp_result(:,1),sigma_imp_result(:,2),'r','LineWidth',2);
hold on;
scatter(VX(:,1),value,'.','b');
axis([300 700 0.01 0.025]);
xlabel('strike price X');
title('comparation between sigma');
legend('sigma theory','sigma experiment values');