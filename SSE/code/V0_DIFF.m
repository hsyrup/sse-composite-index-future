function y=V0_DIFF(a,b,x,x0,T)
    d1=(log(x0/a)+0.5*x^2*T)/(x*sqrt(T));
    d2=(log(x0/a)-0.5*x^2*T)/(x*sqrt(T));
    m1=normcdf(d1,0,1);
    m2=normcdf(d2,0,1);
    V0=x0*m1-a*m2;
    y=V0-b;
%     y=abs(V0-b);
