# SIMULATION & MODELING
## Simulation of the price of SSE Composite Index Future

>Nov. 2015  
>Department of Physics, Fudan University                           
>Supervisor: Prof. Yu Chen from Tokyo University

* Simulated the price of Shanghai Stock Exchange Composite Index Future, according to Bouchard-Sornett's method, successfully displayed the smile curve phenomenon.
* Worked out the function of price under the existence of price changing tendency by fine mathematical theories.

